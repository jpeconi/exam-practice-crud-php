<?php
/**
 * Created by PhpStorm.
 * User: jpeconi
 * Date: 2016-04-19
 * Time: 7:49 PM
 */


// Connect to the database
@ $db = mysqli_connect('localhost', 'root', 'root', 'books');
// Display an error in the event a connection cannot be made
if (mysqli_connect_errno()) {
    echo "Could not connect to database!";
    die;
}

$isbn = mysqli_real_escape_string($db,$_GET['isbn']);

//Create the query
$query = "SELECT * FROM books WHERE isbn = '$isbn'";
// Capture the results of the query into a variable
$bookResult = $db->query($query);
// Get the number of results
$numResults = $bookResult->num_rows;
// Get a row
$row = $bookResult->fetch_assoc();
// Test
//print_r($row);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Search | Assignment 9</title>
    <link href="custom.css" rel="stylesheet">
</head>
<body>
    <div id="bookArea">
        <fieldset>
            <legend>Book Details</legend>
            <h2>Edit the information to be updated</h2>
            <form action="updateBook.php" method="get">
                <lable for="isbn">ISBN:</lable>
                <input type="text" id="isbn" name="isbn" value="<?php echo $isbn?>" readonly>
                <br>
                <lable for="author">Author:</lable>
                <input type="text" id="author" name="author" value="<?php echo $row['author']?>" >
                <br>
                <lable for="title">Title:</lable>
                <input type="text" id="title" name="title" value="<?php echo $row['title']?>" >
                <br>
                <lable for="price">Price:</lable>
                <input type="text" id="price" name="price" value="<?php echo $row['price']?>" >
                <br>
                <input type="submit" value="Update Book">
            </form>
        </fieldset>
       <?php
        // Free the result memory
        $bookResult -> free();
        // Close the connection to the database
        $db -> close();
        include('includeMenu.php');
        ?>
    </div>
</body>
</html>

