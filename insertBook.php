<?php
/**
 * Created by PhpStorm.
 * User: jpeconi
 * Date: 2016-04-19
 * Time: 7:49 PM
 */


// Connect to the database
@ $db = mysqli_connect('localhost', 'root', 'root', 'books');
// Display an error in the event a connection cannot be made
if (mysqli_connect_errno()) {
    echo "Could not connect to database!";
    die;
}

$isbn = mysqli_real_escape_string($db,$_GET['isbn']);
$author = mysqli_real_escape_string($db,$_GET['author']);
$title = mysqli_real_escape_string($db,$_GET['title']);
$price = mysqli_real_escape_string($db,$_GET['price']);

//Create the query
$query = "INSERT INTO books VALUES('$isbn','$author','$title','$price')";
// Capture the results of the query into a variable
$bookResult = $db->query($query);
// Get the number of results
//$numResults = $bookResult->num_rows;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Search | Assignment 9</title>
    <link href="custom.css" rel="stylesheet">
</head>
    <body>
        <div id="bookArea">
            <?php
            if($bookResult) {
                echo "<h1>" . $db->affected_rows . " books was added</h1>";
            } else {
                echo "There was an error!";
            }
            include 'includeMenu.php';
            // Free the result memory
            $bookResult -> free();
            // Close the connection to the database
            $db -> close();
            ?>
        </div>
    </body>
</html>

