<?php
/**
 * Created by PhpStorm.
 * User: jpeconi
 * Date: 2016-04-19
 * Time: 7:49 PM
 */


// Connect to the database
@ $db = mysqli_connect('localhost', 'root', 'root', 'books');
// Display an error in the event a connection cannot be made
if (mysqli_connect_errno()) {
    echo "Could not connect to database!";
    die;
}

$db -> close();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Search | Assignment 9</title>
    <link href="custom.css" rel="stylesheet">
</head>
<body>
<div id="bookArea">
    <fieldset>
        <legend>Book Details</legend>
        <h2>Edit the information to be updated</h2>
        <form action="insertBook.php" method="get">
            <lable for="isbn">ISBN:</lable>
            <input type="text" id="isbn" name="isbn" >
            <br>
            <lable for="author">Author:</lable>
            <input type="text" id="author" name="author">
            <br>
            <lable for="title">Title:</lable>
            <input type="text" id="title" name="title" >
            <br>
            <lable for="price">Price:</lable>
            <input type="text" id="price" name="price">
            <br>
            <input type="submit" value="Add Book">
        </form>
    </fieldset>
    <a href="displayBooks.php">Display Books</a>
</div>
</body>
</html>

