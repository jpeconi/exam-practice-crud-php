<?php
/**
 * Created by PhpStorm.
 * User: jpeconi
 * Date: 2016-04-19
 * Time: 7:49 PM
 */


// Connect to the database
@ $db = mysqli_connect('localhost', 'root', 'root', 'books');
// Display an error in the event a connection cannot be made
if (mysqli_connect_errno()) {
    echo "Could not connect to database!";
    die;
}


//Create the query
$query = "SELECT * FROM books";
// Capture the results of the query into a variable
$bookResult = $db->query($query);
// Get the number of results
$numResults = $bookResult->num_rows;
// Test
//print_r($numResults);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Search | Assignment 9</title>
    <link href="custom.css" rel="stylesheet">
</head>
<body>
<div id="bookArea">
    <?php
    // Logic to determine whether or not to display the data to a table or
    // display an error message to the user. If no results are returned
    // This will display an error message and provide the user to go back
    // and edit their form submission.
    if ($numResults < 1) {
        echo "There was no results found ";
        echo "<br>";
        echo "<a href='javascript:history.back()'>Back</a>";
        echo "</div></body></html>";
        die;
        // If there are results display the data
    } else {
        ?>
        <table id="bookTable">
            <tr>
                <th>ISBN</th>
                <th>Author</th>
                <th>Title</th>
                <th>Price</th>
            </tr>
            <?php
            // Loop which will display each result returned from the query in a table
            while($row = $bookResult->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $row['isbn'];?></td>
                    <td><?php echo $row['author']; ?></td>
                    <td><?php echo $row['title'] ?></td>
                    <td><?php echo $row['price'] ?></td>
                    <td><a href="editBook.php?isbn=<?php echo$row['isbn']?>">Edit</a></td>
                    <td><a href="deleteBook.php?isbn=<?php echo$row['isbn']?>">Delete</a> </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <a href="addBook.php"><h2>Add a Book</h2></a>
        <?php
    }
    // Free the result memory
    $bookResult -> free();
    // Close the conection to the database
    $db -> close();
    ?>
</div>
</body>
</html>

